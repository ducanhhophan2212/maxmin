public class ArrayCalculator{
    public static int maxOfArray(int a[]){
        int max = a[0];
        for(int i = 1; i < a.length; i ++){
            if(a[i] > max) max = a[i];
        }
        return max;
    }

    public static double maxOfArray(double a[]){
        double max = a[0];
        for(int i = 1; i < a.length; i ++){
            if(a[i] > max) max = a[i];
        }
        return max;
    }
    public static int minOfArray(int a[]){
        int min = a[0];
        for(int i = 1; i < a.length; i ++){
            if(a[i] < min) min = a[i];
        }
        return min;
    }

    public static double minOfArray(double a[]){
        double min = a[0];
        for(int i = 1; i < a.length; i ++){
            if(a[i] < min) min = a[i];
        }
        return min;
    }
}